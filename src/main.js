import 'bootstrap';

import App from './App.svelte';
import './assets/styles/app.scss'

const app = new App({
    target: document.body
});

export default app;
