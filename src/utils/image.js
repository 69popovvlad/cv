
import { supportsWebp } from './supportsWebp'

/**
 * @param {Element} node 
 */
export async function image(node) {
  if (!ENV_IS_PROD) return;

  if (supportsWebp) {
    const src = node.getAttribute('src');
    const filename = src.split('.').slice(0, -1).join('.');
    node.setAttribute('src', filename + '.webp');
  }
}
