
import { globalHistory } from "svelte-routing/src/history";

globalHistory.listen(() => window.scrollTo(0, 0))
